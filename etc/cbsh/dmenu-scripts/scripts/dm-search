#!/usr/bin/env bash

# Script Name: dm-search
# Description: A dmenu script to search the web.
# Dependencies: dmenu, dm-scripts
# Gitlab: https://gitlab.com/cbsh-arch/cbsh
# License: https://gitlab.com/cbsh-arch/cbsh/LICENCE
# Contributors: Cameron Miller

# Set Default Browser (Firefox by default, as that is the default installed browser in most Distro's)
#BROWSER="firefox"
#BROWSER="brave"
#BROWSER="qutebrowser"
BROWSER="librewolf"

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

# Setting up the search engine
declare -A websearch
# Syntax = websearch[engineName]="https://www.example.com/search?q="
# You can comment out and add as you need.

# Search Engines
websearch[duckduckgo]="https://duckduckgo.com/?q="
# websearch[brave]="https://search.brave.com/search?q="
websearch[google]="https://www.google.com/search?q="

# News and Information
websearch[bbcnews]="https://www.bbc.co.uk/search?q="
websearch[googlenews]="https://news.google.com/search?q="
websearch[wikipedia]="https://en.wikipedia.org/w/index.php?search="

# Social Media
websearch[youtube]="https://www.youtube.com/results?search_query="
websearch[twitter]="https://twitter.com/search?q="
websearch[reddit]="https://www.reddit.com/search/?q="

# Shopping
websearch[amazon]="https://www.amazon.co.uk/s?k="
websearch[ebay]="https://www.ebay.co.uk/sch/i.html?&_nkw="
# websearch[gumtree]="https://www.gumtree.com/search?search_category=all&q="

# Linux
websearch[archaur]="https://aur.archlinux.org/packages/?O=0&K="
websearch[archpkg]="https://archlinux.org/packages/?sort=&q="
websearch[archwiki]="https://wiki.archlinux.org/index.php?search="

# Development
websearch[github]="https://github.com/search?q="
websearch[gitlab]="https://gitlab.com/search?search="
websearch[stackoverflow]="https://stackoverflow.com/search?q="


# Searching the web
#
# Choosing a Search Engine
engine=$(printf '%s\n' "${!websearch[@]}" | sort | dmenu -h 27 -l 20 -p 'Choose Search Engine:' "$@") || exit 1

# Grabbing the URL
url="${websearch["${engine}"]}"

# Searching with the engine
query=$(echo "$engine" | dmenu -h 27 -l 20 -p 'Enter Search Query:')

query="$(echo "${query}" | jq -s -R -r @uri)"

${BROWSER} "${url}${query}"
